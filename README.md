composer-gather
---------------

It's to clean up legacy codebases using many composers and vendors directories.

It allows you to know what dependencies are required everywhere.

# Usage

```shell
composer-gather [ partial-matching-of-library-name  ]
```

# How to use

git clone this repo, and add it to your path.

# Caveats

This tool depends on gnu find. Never tested it on WSL (Windows Subsystem for Linux).

