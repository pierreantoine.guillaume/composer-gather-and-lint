<?php

declare(strict_types=1);

namespace pag\ComposerGatherAndLint;

use Ds\Sequence;
use Ds\Vector;
use Symfony\Component\Process\Process;

final class ComposerLocator
{
    /**
     * @var string[]
     */
    private array $excludeList = [
        '*/public/*',
        '*/.phpunit/*',
        '*/vendor/*'
    ];

    public function excludeGlob(string $glob): void
    {
        $this->excludeList[] = $glob;
    }

    public function locate(): Sequence
    {
        $command = [
            "find",
            "-name",
            'composer.json',
        ];

        foreach ($this->excludeList as $item) {
            foreach (self::excludeGlobFromGnuFind($item) as $exclusion) {
                $command[] = $exclusion;
            }
        }

        $process =
            new Process($command);
        $process->run();
        return (new Vector(explode("\n", $process->getOutput())))
            ->map([$this, 'trimComposerJsonFromName'])
            ->filter()
            ;
    }

    public static function trimComposerJsonFromName(string $path): ?string
    {
        if ($path === '') {
            return null;
        }
        if ($path === './composer.json') {
            return '.';
        }

        $minNeedle = 0;
        $maxNeedle = strlen($path);

        if (strpos($path, './') === 0) {
            $minNeedle = 2;
        }

        if (strrpos($path, 'composer.json') === strlen($path) - strlen('composer.json')) {
            $maxNeedle -= strlen('composer.json') + 1;
        }
        if ($maxNeedle <= $minNeedle) {
            return null;
        }
        return substr($path, $minNeedle, $maxNeedle - $minNeedle) ?: null;
    }

    public static function excludeGlobFromGnuFind(string $path): array
    {
        return [
            '-not',
            '-path',
            $path,
            '-prune',
        ];
    }
}
