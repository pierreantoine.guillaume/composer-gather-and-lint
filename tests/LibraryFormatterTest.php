<?php

namespace tests\pag\ComposerGatherAndLint;

use pag\ComposerGatherAndLint\Library;
use pag\ComposerGatherAndLint\LibraryFormatter;
use PHPUnit\Framework\TestCase;

class LibraryFormatterTest extends TestCase
{
    public function testFormat(): void
    {
        # Given
        $formatter = new LibraryFormatter();
        $library   = new Library("acme/acme", "1.5", './composer.json');

        # When
        $text = $formatter->format($library);

        # Then
        self::assertSame("Library acme/acme appears 1 time : 1.5 in ./composer.json\n", $text);
    }

    public function testFormatMultipleVersion(): void
    {
        # Given
        $formatter = new LibraryFormatter();
        $library   = new Library("acme/acme", "1.5", './composer.json');
        $library->merge(new Library("acme/acme", "1.5.3", './composer.lock'));
        $library->merge(new Library("acme/acme", "1.5.3", 'folder/composer.json'));

        # When
        $text = $formatter->format($library);

        # Then
        self::assertSame(
            "Library acme/acme appears 3 times (2) :
 ├── ./composer.json 1.5
 ├── ./composer.lock 1.5.3
 └── folder/composer.json 1.5.3\n",
            $text
        );
    }
}
