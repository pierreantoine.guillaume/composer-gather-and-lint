<?php

declare(strict_types=1);

namespace pag\ComposerGatherAndLint;

final class LibraryFormatter
{
    public function format(Library $library): string
    {
        if (count($library->versions) === 1) {
            return sprintf(
                "Library %s appears 1 time : %s in %s\n",
                $library->name,
                array_values($library->versions)[0],
                array_keys($library->versions)[0]
            );
        }

        $result = sprintf(
            "Library %s appears %s times (%s) :\n",
            $library->name,
            count($library->versions),
            count(array_unique($library->versions))
        );

        $counter = 0;
        foreach ($library->versions as $origin => $number) {
            $result .= sprintf(
                " %s %s %s\n",
                ++$counter === count($library->versions) ? '└──' : '├──',
                $origin,
                $number
            );
        }

        return $result;
    }
}
