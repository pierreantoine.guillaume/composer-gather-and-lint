<?php

namespace pag\ComposerGatherAndLint;

use ArrayAccess;
use Ds\Collection;
use Ds\Map;
use Ds\Sequence;
use IteratorAggregate;

final class LibrarySet implements IteratorAggregate, Collection, ArrayAccess
{
    /** @var Map|array<string,Library> */
    private Map $list;

    public function __construct(?Map $list = null)
    {
        $this->list = $list ?: new Map();
    }

    public function merge(Sequence $sequence): void
    {
        foreach ($sequence as $library) {
            if (!isset($this->list[$library->name])) {
                $this->list[$library->name] = $library;
                continue;
            }
            $this->list[$library->name]->merge($library);
        }
    }

    public function clear(): void
    {
        $this->list->clear();
    }

    public function count(): int
    {
        return $this->list->count();
    }

    public function copy(): LibrarySet
    {
        return new self($this->list->copy());
    }

    public function isEmpty(): bool
    {
        return $this->list->isEmpty();
    }

    /**
     * @return array<Library>
     */
    public function toArray(): array
    {
        return $this->list->toArray();
    }

    /**
     * @return iterable<Library>
     */
    public function getIterator()
    {
        foreach ($this->list as $key => $value) {
            yield $key => $value;
        }
        return null;
    }

    public function jsonSerialize(): array
    {
        return $this->list->jsonSerialize();
    }

    public function offsetExists($offset): bool
    {
        return $this->list->offsetExists($offset);
    }

    public function offsetGet($offset)
    {
        return $this->list->offsetGet($offset);
    }

    public function offsetSet($offset, $value): void
    {
        $this->list->offsetSet($offset, $value);
    }

    public function offsetUnset($offset): void
    {
        $this->list->offsetUnset($offset);
    }
}
