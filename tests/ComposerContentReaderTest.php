<?php

declare(strict_types=1);

namespace tests\pag\ComposerGatherAndLint;

use JsonException;
use pag\ComposerGatherAndLint\ComposerContentReader;
use pag\ComposerGatherAndLint\Library;
use PHPUnit\Framework\TestCase;

class ComposerContentReaderTest extends TestCase
{
    /**
     * @throws JsonException
     */
    public function testSourceFromComposerJsonEmpty(): void
    {
        self::assertEmpty(ComposerContentReader::sourceFromComposerJson('dummy', '{"require":{}}'));
    }

    /**
     * @throws JsonException
     */
    public function testSourceFromComposerContentIsFine(): void
    {
        $composerRequirements = ComposerContentReader::sourceFromComposerJson(
            'dummy',
            json_encode(
                [
                    'require' => ["acme/acme" => "5.6.7", "acme2/acme" => "1.3",]
                ],
                JSON_THROW_ON_ERROR
            )
        );
        self::assertCount(2, $composerRequirements);
        self::assertInstanceOf(Library::class, $composerRequirements[0]);
        self::assertInstanceOf(Library::class, $composerRequirements[1]);
        self::assertSame('acme2/acme', $composerRequirements[1]->name);
    }
}
