<?php

namespace pag\ComposerGatherAndLint;

final class Library
{
    public string $name;
    public array $versions;

    public function __construct(string $name, string $version, string $origin)
    {
        $this->name     = $name;
        $this->versions = [$origin => $version];
    }

    public function merge(Library $library): void
    {
        foreach ($library->versions as $origin => $version) {
            $this->versions[$origin] = $version;
        }
    }

    public function hasMultipleVersion(): bool
    {
        return count($this->versions) > 1 && count(array_unique($this->versions)) > 1;
    }

    public static function fromComposerString(string $composerShowString, string $origin): self
    {
        preg_match("#(\S+)\s*(\S+)#", $composerShowString, $matches);
        return new Library($matches[1], $matches[2], $origin);
    }
}
