<?php

declare(strict_types=1);

namespace tests\pag\ComposerGatherAndLint;

use pag\ComposerGatherAndLint\Library;
use PHPUnit\Framework\TestCase;

class LibraryTest extends TestCase
{
    public function testFromComposerString(): void
    {
        $lib = Library::fromComposerString("acme/acme    v1.5     Description", 'composer.json');
        self::assertSame('acme/acme', $lib->name);
        self::assertSame(['composer.json' => 'v1.5'], $lib->versions);
    }
}
