<?php

declare(strict_types=1);

namespace tests\pag\ComposerGatherAndLint;

use pag\ComposerGatherAndLint\ComposerLocator;
use PHPUnit\Framework\TestCase;

final class ComposerLocatorTest extends TestCase
{
    public function testTrimComposerJsonFromNameEmptyGivesNull(): void
    {
        self::assertNull(ComposerLocator::trimComposerJsonFromName(''));
    }

    public function testTrimComposerJsonFromNameBaseGivesDot(): void
    {
        self::assertSame('.', ComposerLocator::trimComposerJsonFromName('./composer.json'));
    }

    public function testTrimComposerJsonFromNameTrimesDotAndFile(): void
    {
        self::assertSame('folder', ComposerLocator::trimComposerJsonFromName('./folder/composer.json'));
    }
}
