<?php

declare(strict_types=1);

namespace pag\ComposerGatherAndLint;

use Ds\Map;
use Ds\Sequence;
use Ds\Vector;
use Symfony\Component\Process\Process;

final class ComposerContentReader
{
    public static function sourceFromComposerJson(string $fileName, string $fileContent): Sequence
    {
        return new Vector(
            (new Map(json_decode($fileContent, true, 512, JSON_THROW_ON_ERROR)["require"]))
                ->map(fn($key, $value) => new Library($key, $value, $fileName))
                ->toArray()
        );
    }

    public function readComposerJson(string $filename): Sequence
    {
        return self::sourceFromComposerJson($filename, file_get_contents($filename));
    }

    public function readComposerLock(string $composerJsonPath): Sequence
    {
        $process = new Process(
            ['composer', '--working-dir=' . $composerJsonPath, 'show']
        );
        $process->mustRun();
        return (new Vector(explode("\n", $process->getOutput())))
            ->filter()
            ->map(fn($value) => Library::fromComposerString($value, "$composerJsonPath/composer.lock"))
            ;
    }
}
